﻿<%@ Page Title="Contact" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Contact.aspx.cs" Inherits="Banca_Bianca.Contact" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <h2><%: Title %>.</h2>
    <h3>Your contact page.</h3>
    <address>
    Banca Bianca DOO<br />
    Krcka Oraščića 4255, Bogatograd, Zamunda<br />
    <abbr title="Phone">Phone, Viber, WhatsApp:</abbr>
    +381 64 12 111 29
</address>

<address>
    <strong>Support:</strong>   <a href="mailto:dusan.paunovic81@gmail.com">Support@Banca.com</a><br />
    <strong>Marketing:</strong> <a href="mailto:dusan.paunovic81@gmail.com">Marketing@Banca.com</a>
</address>
</asp:Content>
